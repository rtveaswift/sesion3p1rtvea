//
//  ProductsViewController.swift
//  sesion3P1
//
//  Created by Pako on 7/11/17.
//  Copyright © 2017 svq.ventures. All rights reserved.
//

import UIKit

class ProductsViewController: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate {

    lazy var collectionView: UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: CGRect.zero, collectionViewLayout: flowLayout)
        cv.delegate = self
        cv.dataSource = self
        cv.collectionViewLayout = flowLayout
        cv.translatesAutoresizingMaskIntoConstraints = false
        setUpCollectionView(cv: cv)
        return cv
    }()
    var products: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpLayout()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK:- setUp methods
    func setUpCollectionView(cv: UICollectionView){
        cv.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "basicCell")
    }
    
    func setUpLayout(){
        self.view.addSubview(collectionView)
        let constraints: [NSLayoutConstraint] = [
            collectionView.topAnchor.constraint(equalTo: self.view.topAnchor),
            collectionView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            collectionView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
    func setUpCell(cell: UICollectionViewCell, forIndexPath indexPath: IndexPath){

    }
    //MARK:- UICollectionView delegate methods
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let aCell: UICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "basicCell", for: indexPath)
        setUpCell(cell: aCell, forIndexPath: indexPath)
        return aCell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellSize: CGSize = CGSize(width: collectionView.bounds.width / 2, height: collectionView.bounds.height / 4 )
        return cellSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(products[indexPath.row])
    }
}
