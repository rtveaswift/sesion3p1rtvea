//
//  SalesUIViewController.swift
//  sesion3P1
//
//  Created by Pako on 7/11/17.
//  Copyright © 2017 svq.ventures. All rights reserved.
//

import UIKit

class SalesViewController: ProductsViewController {

    let tabBarHeight: CGFloat = 64
    override func viewDidLoad() {
        super.viewDidLoad()
        products = ["tie", "boW", "hat", "tie"]
        // Do any additional setup after loading the view.
        self.navigationController?.hidesBarsOnSwipe = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func setUpCell(cell: UICollectionViewCell, forIndexPath indexPath: IndexPath) {
        if let theCell = cell as? SalesCollectionViewCell{
            switch indexPath.row % 3{
            case 0:
                theCell.image = UIImage(named: "griller")
                theCell.labelText = products[indexPath.row]
            case 1:
                theCell.image = UIImage(named: "hotdog")
                theCell.labelText = products[indexPath.row]
            case 2:
                theCell.image = UIImage(named: "server")
                theCell.labelText = products[indexPath.row]
            default: break
            }
        }
        
    }
    override func setUpCollectionView(cv: UICollectionView) {
        cv.register(SalesCollectionViewCell.self, forCellWithReuseIdentifier: SalesCollectionViewCell.cellID)
        cv.showsVerticalScrollIndicator = false
        cv.isPagingEnabled = true
    
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let aCell: UICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: SalesCollectionViewCell.cellID, for: indexPath) as? SalesCollectionViewCell ?? UICollectionViewCell()
        setUpCell(cell: aCell, forIndexPath: indexPath)
        return aCell
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.bounds.width, height: self.view.bounds.height - tabBarHeight)
//This is to show audience what collecitonviewlayout is about
//        switch indexPath.row % 3{
//        case 0:
//            return CGSize(width: self.view.bounds.width / 2, height: self.view.bounds.height - tabBarHeight)
//        case 1:
//            return CGSize(width: self.view.bounds.width, height: self.view.bounds.height)
//        case 2:
//            return CGSize(width: self.view.bounds.width / 3, height: self.view.bounds.height / 4)
//        default:
//            return CGSize(width: self.view.bounds.width, height: self.view.bounds.height - tabBarHeight)
//
//        }
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    
}
