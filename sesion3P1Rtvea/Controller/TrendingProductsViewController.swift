//
//  TrendingProductsViewController.swift
//  sesion3P1
//
//  Created by Pako on 7/11/17.
//  Copyright © 2017 svq.ventures. All rights reserved.
//

import UIKit

class TrendingProductsViewController: ProductsViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()
        products = ["hat","tie","bow"]
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func setUpCollectionView(cv: UICollectionView) {
        super.setUpCollectionView(cv: cv)
        cv.register(TrendingCollectionViewCell.self, forCellWithReuseIdentifier: TrendingCollectionViewCell.cellID)
    }

    override func setUpLayout() {
        super.setUpLayout()
    }
    
    override func setUpCell(cell: UICollectionViewCell, forIndexPath indexPath: IndexPath) {
        super.setUpCell(cell: cell, forIndexPath: indexPath)
        guard let cellP = cell as? TrendingCollectionViewCell else { return }
        switch indexPath.row % 3{
        case 0:
            cellP.image = UIImage(named: "griller")
            cellP.labelText = products[indexPath.row]
        case 1:
            cellP.image = UIImage(named: "hotdog")
            cellP.labelText = products[indexPath.row]
        case 2:
            cellP.image = UIImage(named: "server")
            cellP.labelText = products[indexPath.row]
        default: break
        }
    }

    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellSize: CGSize = CGSize(width: collectionView.bounds.width / 2, height: collectionView.bounds.height / 4 )
        return cellSize
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let aCell: UICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: TrendingCollectionViewCell.cellID, for: indexPath) as? TrendingCollectionViewCell ?? UICollectionViewCell()
        if let cell = aCell as? TrendingCollectionViewCell{
            setUpCell(cell: cell, forIndexPath: indexPath)
            return cell
        }
        
        return aCell
    }
    
}
