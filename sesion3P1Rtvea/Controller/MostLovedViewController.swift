//
//  MostLovedViewController.swift
//  sesion3P1
//
//  Created by Pako on 7/11/17.
//  Copyright © 2017 svq.ventures. All rights reserved.
//

import UIKit

class MostLovedViewController: ProductsViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        products = ["tie","hat"]
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func setUpCollectionView(cv: UICollectionView) {
        super.setUpCollectionView(cv: cv)
        cv.register(MostLovedCollectionViewCell.self, forCellWithReuseIdentifier: MostLovedCollectionViewCell.cellID)
    }
    
    override func setUpCell(cell: UICollectionViewCell, forIndexPath indexPath: IndexPath) {
        if let cellP = cell as? MostLovedCollectionViewCell{
            switch indexPath.row % 3{
            case 0:
                cellP.image = UIImage(named: "griller")
                cellP.labelText = products[indexPath.row]
            case 1:
                cellP.image = UIImage(named: "hotdog")
                cellP.labelText = products[indexPath.row]
            case 2:
                cellP.image = UIImage(named: "server")
                cellP.labelText = products[indexPath.row]
            default: break
            }
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let aCell = collectionView.dequeueReusableCell(withReuseIdentifier: MostLovedCollectionViewCell.cellID, for: indexPath)
        setUpCell(cell: aCell, forIndexPath: indexPath)
        return aCell
    }
    

}
