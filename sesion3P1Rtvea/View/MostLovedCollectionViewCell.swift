//
//  MostLovedCollectionViewCell.swift
//  sesion3P1
//
//  Created by Pako on 7/11/17.
//  Copyright © 2017 svq.ventures. All rights reserved.
//

import UIKit

class MostLovedCollectionViewCell: ProductCollectionViewCell {
    override class var cellID: String {
        get{
            return "mostLovedCell"
        }
    }
}
