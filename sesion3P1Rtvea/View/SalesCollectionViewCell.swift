//
//  SalesCollectionViewCell.swift
//  sesion3P1
//
//  Created by Pako on 7/11/17.
//  Copyright © 2017 svq.ventures. All rights reserved.
//

import UIKit

class SalesCollectionViewCell: ProductCollectionViewCell {
    override class var cellID: String {
        get{
            return "salesCell"
        }
    }
    
    lazy var textView: UITextView = {
       let textView = UITextView()
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.textAlignment = .center
        textView.font = UIFont.boldSystemFont(ofSize: 18)
        textView.isEditable = false
        textView.isUserInteractionEnabled = true
        return textView
    }()
    
    override var labelText: String?{
        didSet{
            textView.text = labelText
        }
    }
    override func setUpLayout() {
        clipsToBounds = true
        backgroundColor = .white
        layer.cornerRadius = 12
        layer.borderWidth = 3
        layer.borderColor = UIColor.white.cgColor
        contentView.addSubview(textView)
        contentView.addSubview(imageView)
        contentView.addConstraint(NSLayoutConstraint(item: imageView, attribute: .centerX, relatedBy: .equal, toItem: contentView, attribute: .centerX, multiplier: 1, constant: 0))
        contentView.addConstraint(NSLayoutConstraint(item: imageView, attribute: .centerY, relatedBy: .equal, toItem: contentView, attribute: .centerY, multiplier: 1, constant: 0))
        imageView.heightAnchor.constraint(equalToConstant: 72).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 72).isActive = true
        
        contentView.addConstraint(NSLayoutConstraint(item: textView, attribute: .leading, relatedBy: .equal, toItem: contentView, attribute: .leading, multiplier: 1, constant: 4))
        contentView.addConstraint(NSLayoutConstraint(item: textView, attribute: .trailing, relatedBy: .equal, toItem: contentView, attribute: .trailing, multiplier: 1, constant: -4))
        contentView.addConstraint(NSLayoutConstraint(item: textView, attribute: .bottom, relatedBy: .equal, toItem: contentView, attribute: .bottom, multiplier: 1, constant: -4))
        contentView.addConstraint(NSLayoutConstraint(item: textView, attribute: .top, relatedBy: .lessThanOrEqual, toItem: imageView, attribute: .bottom, multiplier: 1, constant: 4))
        
        
        
        
    }
}
