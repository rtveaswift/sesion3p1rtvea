//
//  ProductCollectionViewCell.swift
//  sesion3P1
//
//  Created by Pako on 7/11/17.
//  Copyright © 2017 svq.ventures. All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {
    class var cellID: String {
        get{
            return "productCell"
        }
    }
    var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    var image: UIImage? {
        didSet{
            imageView.image = image?.withRenderingMode(.alwaysOriginal)
       
        }
    }
    var labelText: String? {
        didSet{
            descriptionLabel.text = labelText
        }
    }
    private lazy var descriptionLabel: UILabel = {
       let descriptionLabel = UILabel()
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.textAlignment = .center
        descriptionLabel.font = UIFont.boldSystemFont(ofSize: 18)
        return descriptionLabel
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUpLayout()
    }
    
    func setUpLayout(){
        backgroundColor = .white
        
        contentView.addSubview(imageView)
        contentView.addSubview(descriptionLabel)
//        imageView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
//        imageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        imageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8).isActive = true
        imageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8).isActive = true
//
        descriptionLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8).isActive = true
        descriptionLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8).isActive = true

       
       // contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-4-[imageView]-4-|", options: NSLayoutFormatOptions(), metrics: nil, views: ["imageView": imageView]))
       // contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-4-[descriptionLabel]-4-|", options: NSLayoutFormatOptions(), metrics: nil, views: ["descriptionLabel": descriptionLabel]))
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[imageView]-8-[descriptionLabel]-4-|", options: NSLayoutFormatOptions(), metrics: nil, views: ["imageView": imageView, "descriptionLabel" : descriptionLabel]))
    }
}
